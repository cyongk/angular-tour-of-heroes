import {Component, Input, OnInit} from '@angular/core';
import {Hero} from "../hero";
import {ActivatedRoute} from "@angular/router";
import {HeroService} from "../hero.service";
import {Location} from '@angular/common';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {

  /**
   * 注入属性
   * 路由模式可以不用这种模式
   */
  // @Input() hero?: Hero;

  /**
   * 路由模式修改成这样
   */
  hero: Hero | undefined;

  /**
   * ActivatedRoute 保存着到这个 HeroDetailComponent 实例的路由信息,这个组件对从 URL 中提取的路由参数感兴趣
   * location 是一个 Angular 的服务，用来与浏览器打交道。 稍后，你就会使用它来导航回上一个视图。
   * @param route
   * @param heroService
   * @param location
   */
  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location) {
  }

  ngOnInit(): void {
    this.getHero();
  }

  /**
   * route.snapshot 是一个路由信息的静态快照，抓取自组件刚刚创建完毕之后。
   * paramMap 是一个从 URL 中提取的路由参数值的字典。 "id" 对应的值就是要获取的英雄的 id。
   * 路由参数总会是字符串。 JavaScript 的 Number 函数会把字符串转换成数字，英雄的 id 就是数字类型。
   *
   */
  getHero(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.heroService.getHero(id)
      .subscribe(hero => this.hero = hero);
  }

  /**
   * 告诉浏览器退回上一页
   */
  goBack(): void {
    this.location.back();
  }

  /**
   * 修改英雄对象
   */
  save(): void {
    if (this.hero) {
      this.heroService.updateHero(this.hero)
        .subscribe(() => this.goBack());
    }
  }

}
