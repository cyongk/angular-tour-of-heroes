import { Injectable } from '@angular/core';

/**
 * 消息服务类
 */
@Injectable({
  providedIn: 'root'
})
export class MessageService {

  /**
   * 初始化消息数组
   */
  messages: string[] = [];

  constructor() { }

  /**
   * 往数据组追加消息
   * @param message
   */
  add(message: string) {
    this.messages.push(message);
  }

  /**
   * 清理数组中消息
   */
  clear() {
    this.messages = [];
  }
}
