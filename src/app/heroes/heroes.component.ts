import {Component, OnInit} from '@angular/core';
import {Hero} from "../hero";
import {HEROES} from '../mock-heroes';
import {HeroService} from "../hero.service";
import {Observable, of} from "rxjs";
import {MessageService} from "../message.service";

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes: Hero[] = [];

  selectedHero?: Hero;

  constructor(private heroService: HeroService, private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.getHeroes();
  }

  /**
   * 路由模式可以不使用此方法
   * @param hero
   */
  onSelect(hero: Hero): void {
    this.selectedHero = hero;
    this.messageService.add(`HeroesComponent: Selected hero id=${hero.id}`);
  }

  /**
   * Original
   */
  getHeroes1(): void {
    //this.heroes = this.heroService.getHeroes1();
  }

  /**
   * Observable
   */
  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes);
  }

  /**
   * 添加英雄
   * 当 addHero() 保存成功时，subscribe() 的回调函数会收到这个新英雄，并把它追加到 heroes 列表中以供显示
   * @param name
   */
  add(name: string): void {
    name = name.trim();
    if (!name) {
      return;
    }
    this.heroService.addHero({name} as Hero)
      .subscribe(hero => {
        this.heroes.push(hero);
      });
  }

  /**
   * 删除英雄
   * delete() 方法会在 HeroService 对服务器的操作成功之前，先从列表中移除要删除的英雄。
   * 如果你忘了调用 subscribe()，本服务将不会把这个删除请求发送给服务器。 作为一条通用的规则，Observable 在有人订阅之前什么都不会做。
   * @param hero
   */
  delete(hero: Hero): void {
    this.heroes = this.heroes.filter(h => h !== hero);
    this.heroService.deleteHero(hero.id).subscribe();
  }

}
