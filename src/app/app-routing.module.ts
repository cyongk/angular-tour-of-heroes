import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {HeroesComponent} from "./heroes/heroes.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {HeroDetailComponent} from "./hero-detail/hero-detail.component";

/**
 * 路由配置
 * path: 用来匹配浏览器地址栏中 URL 的字符串
 * component: 导航到该路由时，路由器应该创建的组件
 */
const routes: Routes = [

  // 当应用启动时，浏览器的地址栏指向了网站的根路径。 它没有匹配到任何现存路由，因此路由器也不会导航到任何地方。
  // 默认路由，这个路由会把一个与空路径“完全匹配”的 URL 重定向到路径为 '/dashboard' 的路由。
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'heroes', component: HeroesComponent},
  // 参数化路由,path 中的冒号（:）表示 :id 是一个占位符
  {path: 'detail/:id', component: HeroDetailComponent}
];

/**
 * 路由模块
 */
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
